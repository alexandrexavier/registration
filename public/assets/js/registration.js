/**
 * It returns a strength score from 0-5 for the password given, 0 being not secure and 5 very secure.
 *
 * If a password contains less than 6 characters, its score is 0.
 * After that, the algorithm counts the existence of the following patterns:
 *
 * - Numeric characters
 * - Lowercase alphabetical characters
 * - Uppercase alphabetical characters
 * - Special characters
 * - At least 12 characters
 *
 * @param {String} password
 *
 * @returns {number} [0-5]
 */
function classify_password_strength(password) {
    if ('undefined' == typeof password || null == password || password.length < 6) {
        return 0;
    }

    var strength = 0;

    // A sort of pattern to be tested
    var patterns = [
        /[0-9]/,        // Numeric characters
        /[a-z]/,        // Lowercase alphabetical characters
        /[A-Z]/,        // Uppercase alphabetical characters
        /[^A-z-0-9]/i,  // Special characters
        /^.{12,}$/      // At least 12 characters
    ];

    for (var i in patterns) {
        if (patterns[i].test(password)) {
            console.debug(patterns[i]);
            strength++
        }
    }

    return strength;
}

/**
 *


 * Defining Angular JS application.
 */
var app = angular.module('app', ['ngMessages'])
    .controller('registrationController', function($scope, $http) {
        $scope.passwordStrength = function() {
            return classify_password_strength($scope.password);
        };

        $scope.submitForm = function() {
            var req = {
                method: 'POST',
                url: '/api/registration',
                headers : {
                    'Content-Type': 'application/jsonrequest'
                },
                data: {
                    email: $scope.email,
                    password: $scope.password,
                    name: $scope.name,
                    nickname: $scope.nickname,
                    address: $scope.address,
                    region: $scope.region,
                    country: $scope.country,
                    phone: $scope.phone,
                    nif: $scope.nif
                }
            };

            $scope.errorMessage = '';

            $http(req).then(function (response) {
                    // On success
                    if (typeof response.data !== 'undefined'
                        && typeof response.data.errors !== 'undefined'
                    ) {
                        for (error in response.data.errors) {
                            // @todo Put messages in each corresponding scope error var
                            $scope.errorMessage += "\n " + response.data.errors[error];
                        }
                    }

                    if (null == response.data.errors) {
                        var answer = confirm('Registrado com sucesso, obrigado!')

                        if (answer) {
                            location.reload();
                        }
                    }
                },
                function (response) {
                    // On error
                    console.log('Unable to post registration');
                    $scope.errorMessage = 'Ocorreu algum ao criar o seu registro, tente novamente mais tarde.'
                });
        };
    })
    .directive('sameAs', function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ngModel) {
                ngModel.$parsers.unshift(validate);

                // Force-trigger the parsing pipeline.
                scope.$watch(attrs.sameAs, function () {
                    ngModel.$setViewValue(ngModel.$viewValue);
                });

                function validate(value) {
                    var isValid = scope.$eval(attrs.sameAs) == value;

                    ngModel.$setValidity('same-as', isValid);

                    return isValid ? value : undefined;
                }
            }
        };
    })
    .directive('emailNotUsed', function($http, $q) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.emailNotUsed = function(modelValue, viewValue) {
                    return $http.get('/api/registration/email/' + viewValue, viewValue).then(function(response) {
                        if (typeof response.data == 'undefined'
                            || typeof response.data.alreadyInUse == 'undefined'
                        ) {
                            console.log('Unable to validate if the Email is already in use');

                            return true;
                        }

                        return response.data.alreadyInUse ? $q.reject('Email is already in use.') : true
                    });
                };
            }
        };
    });
