<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/
require_once __DIR__
    . DIRECTORY_SEPARATOR . '..'
    . DIRECTORY_SEPARATOR . 'vendor'
    . DIRECTORY_SEPARATOR . 'autoload.php';

/*
|--------------------------------------------------------------------------
| Building and Running the Application
|--------------------------------------------------------------------------
|
| We are using the package "league/route" for handle our HTTP requests
| according to the PSR.
|
*/
use AlexandreXavier\App\Bootstrap;
use League\Container\Container;

(new Bootstrap(new Container()))
    ->webSetup()
    ->handleHttpRequest();
