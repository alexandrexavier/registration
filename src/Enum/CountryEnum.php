<?php

namespace AlexandreXavier\Registration\Enum;

/**
 * Class CountryEnum
 *
 * @package AlexandreXavier\Enum
 */
class CountryEnum
{
    const ISO_CODE_AO = 'AO';
    const ISO_CODE_BR = 'BR';
    const ISO_CODE_CV= 'CV';
    const ISO_CODE_PT = 'PT';

    const EN_NAME_AO = 'Angola';
    const EN_NAME_BR = 'Brazil';
    const EN_NAME_CAPE_VERDE= 'Cape Verde';
    const EN_NAME_PT = 'Portugal';

    const NATIVE_NAME_AO = 'Angola';
    const NATIVE_NAME_BR = 'Brasil';
    const NATIVE_NAME_CV= 'Cabo Verde';
    const NATIVE_NAME_PT = 'Portugal';
}
