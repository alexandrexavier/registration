<?php

namespace AlexandreXavier\Registration\Controller;

/**
 * @package AlexandreXavier\Registration\Controller
 */
class CountryController extends AbstractController
{
    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function registrationAction()
    {
        return $this->buildJsonResponse($this->getCountryRepo()->getCountriesForRegistrationForm());
    }

    /**
     * @return \AlexandreXavier\Registration\Repository\CountryRepo
     */
    private function getCountryRepo()
    {
        return $this->container->get('repo.country');
    }
}
