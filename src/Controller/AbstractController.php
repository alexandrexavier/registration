<?php

namespace AlexandreXavier\Registration\Controller;

use League\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @package AlexandreXavier\Registration\Controller
 */
abstract class AbstractController
{
    /**
     * @var \League\Container\ContainerInterface
     */
    protected $container;

    /**
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    protected $request;

    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    protected $response;

    /**
     * @var string
     */
    protected $viewDirectory;

    /**
     * @param \League\Container\ContainerInterface     $container
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface      $response
     * @param string                                   $viewDirectory
     */
    public function __construct(
        ContainerInterface $container,
        ServerRequestInterface $request,
        ResponseInterface $response,
        $viewDirectory
    ) {
        $this->container = $container;
        $this->request = $request;
        $this->response = $response;
        $this->viewDirectory = $viewDirectory;
    }

    /**
     * @param array   $body
     * @param integer $responseCode [optional] default 200
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function buildJsonResponse(array $body, $responseCode = 200)
    {
        $this->response->getBody()->write(
            json_encode($body)
        );

        return $this->response->withStatus($responseCode);
    }

    /**
     * @param string  $viewFile
     * @param array   $placeholders [optional]
     * @param integer $responseCode [optional] default 200
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function buildHtmlResponse($viewFile, $placeholders = [], $responseCode = 200)
    {
        $this->response->getBody()->write(
            $this->renderView($viewFile, $placeholders)
        );

        return $this->response->withStatus($responseCode);
    }

    /**
     * @param string $viewFile
     * @param array  $placeholders [optional]
     *
     * @return string
     */
    protected function renderView($viewFile, $placeholders = [])
    {
        if ([] == $placeholders) {
            return $this->getViewContent($viewFile);
        }

        return strtr($this->getViewContent($viewFile), $placeholders);
    }

    /**
     * @param string $viewFile
     *
     * @return string
     */
    protected function getViewContent($viewFile)
    {
        $filePath = "{$this->viewDirectory}{$viewFile}";

        if (!file_exists($filePath)) {
            throw new \LogicException("Unable to read the file: {$viewFile}");
        }

        return file_get_contents($filePath);
    }
}
