<?php

namespace AlexandreXavier\Registration\Controller;

use AlexandreXavier\Registration\Service\RegistrationValidator;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;
use League\Route\Http\Exception\BadRequestException;

/**
 * @package AlexandreXavier\Registration\Controller
 */
class RegistrationController extends AbstractController
{
    /**
     * @throws \League\Route\Http\Exception\BadRequestException
     */
    public function postIndexAction()
    {
        try {
            $requestParams = json_decode(file_get_contents('php://input'), true);
        } catch (Exception $exception) {
            throw new BadRequestException('Request Payload must be a JSON');
        }

        $validationErrors = (new RegistrationValidator())->getValidationErrors($requestParams);

        if (!empty($validationErrors)) {
            return $this->buildJsonResponse(['errors' => $validationErrors]);
        }

        $builder = $this->getRegistrationBuilder();

        $registration = $builder->buildRegistration($requestParams);

        try {
            $this->getRegistrationRepo()
                ->creation(
                    $registration,
                    $builder->buildAddressOrNull($registration, $requestParams),
                    $builder->buildRegistrationNifOrNull($registration, $requestParams)
                );
        } catch (UniqueConstraintViolationException $uniqueConstraintViolationException) {
            $validationErrors['email'] = 'Conta já cadastrada ' . $uniqueConstraintViolationException->getMessage();
        }

        return $this->buildJsonResponse([
            'id' => $registration->getId(),
            'errors' => $validationErrors,
        ]);
    }

    /**
     * @param string $email
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function checkEmailAction($email)
    {
        return $this->buildJsonResponse([
            'alreadyInUse' =>
                $this->getRegistrationRepo()->findIdByEmail($email) > 0,
        ]);
    }

    /**
     * @return \AlexandreXavier\Registration\Service\RegistrationBuilder
     */
    private function getRegistrationBuilder()
    {
        return $this->container->get('registration_builder');
    }

    /**
     * @return \AlexandreXavier\Registration\Repository\RegistrationRepo
     */
    private function getRegistrationRepo()
    {
        return $this->container->get('repo.registration');
    }
}
