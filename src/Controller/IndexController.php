<?php

namespace AlexandreXavier\Registration\Controller;

/**
 * @package AlexandreXavier\Registration\Controller
 */
class IndexController extends AbstractController
{
    /**
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function indexAction()
    {
        return $this->buildHtmlResponse(
            'registration.html',
            [
                '<<%PLACEHOLDER_COUNTRIES_JSON%>>' =>
                    json_encode(
                        $this->getCountryRepo()->getCountriesForRegistrationForm()
                    ),
            ]
        );
    }

    /**
     * @return \AlexandreXavier\Registration\Repository\CountryRepo
     */
    private function getCountryRepo()
    {
        return $this->container->get('repo.country');
    }
}
