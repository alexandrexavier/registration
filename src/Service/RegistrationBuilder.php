<?php

namespace AlexandreXavier\Registration\Service;

use AlexandreXavier\Registration\Entity\Address;
use AlexandreXavier\Registration\Entity\Country;
use AlexandreXavier\Registration\Entity\Registration;
use AlexandreXavier\Registration\Entity\RegistrationNif;
use AlexandreXavier\Registration\Repository\CountryRepo;
use League\Route\Http\Exception\BadRequestException;

/**
 * @package AlexandreXavier\Registration\Service
 */
class RegistrationBuilder
{
    /**
     * @var \AlexandreXavier\Registration\Repository\CountryRepo
     */
    private $countryRepo;

    /**
     * @param \AlexandreXavier\Registration\Repository\CountryRepo $countryRepo
     */
    public function __construct(CountryRepo $countryRepo)
    {
        $this->countryRepo = $countryRepo;
    }

    /**
     * @param array $requestParameters
     *
     * @return \AlexandreXavier\Registration\Entity\Registration
     *
     * @throws \League\Route\Http\Exception\BadRequestException
     */
    public function buildRegistration(array $requestParameters)
    {
        $country = $this->countryRepo->findOneBy(['isoCode' => $requestParameters['country']]);

        if (!($country instanceof Country)) {
            throw new BadRequestException('país inválido');
        }

        $registration = (new Registration())
            ->setCountry($country)
            ->setPassword(
                hash(
                    'sha256',
                    strrev($requestParameters['password'])
                )
            )
            ->setEmail(trim($requestParameters['email']))
            ->setName($requestParameters['name'])
            ->setNickname($requestParameters['nickname'])
        ;

        if (array_key_exists('phone', $requestParameters)) {
            $registration->setPhone($requestParameters['phone']);
        }

        return $registration;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Registration $registration
     * @param array                                             $requestParameters
     *
     * @return \AlexandreXavier\Registration\Entity\Address|null
     */
    public function buildAddressOrNull(Registration $registration, array $requestParameters)
    {
        $address = null;

        foreach (['addressLine', 'region', 'postcode'] as $fieldName) {
            if (empty($requestParameters[$fieldName])) {
                continue;
            }

            if (!($address instanceof Address)) {
                $address =  new Address($registration);
            }

            switch ($fieldName) {
                case 'addressLine':
                    $address->setAddressLine($requestParameters[$fieldName]);
                    break;
                case 'region':
                    $address->setRegion($requestParameters[$fieldName]);
                    break;
                case 'postcode':
                    $address->setPostcode($requestParameters[$fieldName]);
                    break;
            }
        }

        return $address;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Registration $registration
     * @param array                                             $requestParameters
     *
     * @return \AlexandreXavier\Registration\Entity\RegistrationNif|null
     */
    public function buildRegistrationNifOrNull(Registration $registration, array $requestParameters)
    {
        if (empty($requestParameters['nif'])) {
            return null;
        }

        return (new RegistrationNif($registration))->setNif($requestParameters['nif']);
    }
}
