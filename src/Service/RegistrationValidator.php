<?php

namespace AlexandreXavier\Registration\Service;

use AlexandreXavier\Registration\Enum\CountryEnum;

/**
 * @package AlexandreXavier\Registration\Controller
 */
class RegistrationValidator
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @param array $requestParams
     */
    public function getValidationErrors(array $requestParams)
    {
        foreach ($this->getMandatoryFields() as $mandatoryField) {
            if (empty($requestParams[$mandatoryField])) {
                $this->addError(
                    $mandatoryField,
                    "{$mandatoryField} is mandatory"
                );
            }
        }

        $this->validateNif($requestParams);

        $this->validatePostcode($requestParams);
    }

    /**
     * @param array $requestParams
     */
    private function validateNif(array $requestParams)
    {
        if (CountryEnum::ISO_CODE_PT != $requestParams['country']) {
            return;
        }

        if (empty($requestParams['nif'])) {
            $this->addError('nif', 'nif is mandatory');

            return;
        }

        if (!is_numeric($requestParams['nif'])
            || preg_match('/^[0-9]{9}$/', $requestParams['nif'])
        ) {
            $this->addError('nif', 'formato invalido');
        }
    }

    /**
     * @param array $requestParams
     */
    private function validatePostcode(array $requestParams)
    {
        if (!empty($requestParams['postcode'])
            && !preg_match('/^[0-9]{4}[-][0-9]{3}$/', $requestParams['postcode'])
        ) {
            $this->addError('nif', 'formato invalido');
        }
    }

    /**
     * @return string[]
     */
    private function getMandatoryFields()
    {
        return ['email', 'password', 'name', 'nickname', 'country'];
    }

    /**
     * @param string $field
     * @param string $errorMessage
     */
    private function addError($field, $errorMessage)
    {
        if (!array_key_exists($field, $this->errors)) {
            $this->errors[$field] = [];
        }

        $this->errors[$field][] = $errorMessage;
    }
}
