<?php

namespace AlexandreXavier\Registration\Repository;

use AlexandreXavier\Registration\Entity\Address;
use AlexandreXavier\Registration\Entity\Registration;
use AlexandreXavier\Registration\Entity\RegistrationNif;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Exception;

/**
 * repo.registration
 *
 * @package AlexandreXavier\Registration\Repository
 */
class RegistrationRepo extends EntityRepository
{
    /**
     * @param string $email
     *
     * @return integer|null
     */
    public function findIdByEmail($email)
    {
        try {
            return $this->createQueryBuilder('r')
                ->select(['r.id'])
                ->where('r.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (NoResultException $noResultException) {
            return null;
        }
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Registration         $registration
     * @param \AlexandreXavier\Registration\Entity\Address|null         $address      [optional]
     * @param \AlexandreXavier\Registration\Entity\RegistrationNif|null $nif          [optional]
     *
     * @throws \Exception
     */
    public function creation(
        Registration $registration,
        Address $address = null,
        RegistrationNif $nif = null
    ) {
        $this->getEntityManager()
            ->getConnection()
            ->beginTransaction();

        try {
            $this->getEntityManager()->persist($registration);

            $this->getEntityManager()->flush();

            if ($address instanceof Address) {
                $this->getEntityManager()->persist($address);
            }

            if ($nif instanceof RegistrationNif) {
                $this->getEntityManager()->persist($nif);
            }

            $this->getEntityManager()->flush();

            $this->getEntityManager()
                ->getConnection()
                ->commit();
        } catch (Exception $exception) {
            $this->getEntityManager()
                ->getConnection()
                ->rollBack();

            throw $exception;
        }
    }
}
