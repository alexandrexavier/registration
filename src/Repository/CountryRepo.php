<?php

namespace AlexandreXavier\Registration\Repository;

use AlexandreXavier\Registration\Entity\Country;
use AlexandreXavier\Registration\Entity\RegistrationCountry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * repo.country
 *
 * @package AlexandreXavier\Registration\Repository
 */
class CountryRepo extends EntityRepository
{
    /**
     *  SELECT
     *      c.iso_code,
     *      c.nativeName
     *  FROM
     *      country AS c
     *  INNER JOIN
     *      registration_country AS rc ON rc.fk_country = c.id
     *
     * @return array
     */
    public function getCountriesForRegistrationForm()
    {
        return $this->getEntityManager()
            ->createQuery(
                sprintf(
                    'SELECT c.isoCode, c.nativeName FROM %s rc JOIN %s c WITH rc.country = c ORDER BY c.isoCode DESC',
                    RegistrationCountry::class,
                        Country::class
                )
            )
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
