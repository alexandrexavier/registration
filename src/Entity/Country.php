<?php

namespace AlexandreXavier\Registration\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AlexandreXavier\Registration\Repository\CountryRepo")
 *
 * @ORM\Table(
 *     name="country",
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="A_COUNTRY_ISO_CODE_MUST_BE_UNIQUE", columns={"iso_code"})
 *     }
 * )
 *
 * @package AlexandreXavier\Registration\Entity
 */
class Country
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="iso_code", type="string", length=2, nullable=false, options={"fixed" = true})
     *
     * @var string
     */
    private $isoCode;

    /**
     * @ORM\Column(name="en_name", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $enName;

    /**
     * @ORM\Column(name="native_name", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $nativeName;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsoCode()
    {
        return $this->isoCode;
    }

    /**
     * @param string $isoCode
     *
     * @return $this
     */
    public function setIsoCode($isoCode)
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnName()
    {
        return $this->enName;
    }

    /**
     * @param string $enName
     *
     * @return $this
     */
    public function setEnName($enName)
    {
        $this->enName = $enName;

        return $this;
    }

    /**
     * @return string
     */
    public function getNativeName()
    {
        return $this->nativeName;
    }

    /**
     * @param string $nativeName
     *
     * @return $this
     */
    public function setNativeName($nativeName)
    {
        $this->nativeName = $nativeName;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
