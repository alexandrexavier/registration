<?php

namespace AlexandreXavier\Registration\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="address")
 *
 * @package AlexandreXavier\Registration\Entity
 */
class Address
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AlexandreXavier\Registration\Entity\Registration")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_registration", referencedColumnName="id", nullable=false, unique=true)
     * })
     *
     * @var \AlexandreXavier\Registration\Entity\Registration
     */
    private $registration;

    /**
     * @ORM\Column(name="address_line", type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $addressLine;

    /**
     * @ORM\Column(name="region", type="string", length=45, nullable=true)
     *
     * @var string
     */
    private $region;

    /**
     * @ORM\Column(name="postcode", type="string", length=32, nullable=true)
     *
     * @var string
     */
    private $postcode;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * Address constructor.
     *
     * @param \AlexandreXavier\Registration\Entity\Registration $registration
     */
    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
        $this->createdAt = $this->updatedAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \AlexandreXavier\Registration\Entity\Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Registration $registration
     *
     * @return $this
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddressLine()
    {
        return $this->addressLine;
    }

    /**
     * @param string $addressLine
     *
     * @return $this
     */
    public function setAddressLine($addressLine)
    {
        $this->addressLine = $addressLine;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     *
     * @return $this
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     *
     * @return $this
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
     
        return $this;
    }
}
