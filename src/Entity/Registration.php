<?php

namespace AlexandreXavier\Registration\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AlexandreXavier\Registration\Repository\RegistrationRepo")
 *
 * @ORM\Table(
 *     name="registration",
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="A_REGISTRATION_EMAIL_MUST_BE_UNIQUE", columns={"email"})
 *     }
 * )
 *
 * @package AlexandreXavier\Registration\Entity
 */
class Registration
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AlexandreXavier\Registration\Entity\Country")
     * @ORM\JoinColumns({@ORM\JoinColumn(name="fk_country", referencedColumnName="id")})
     *
     * @var \AlexandreXavier\Registration\Entity\Country
     */
    private $country;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     *
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(name="password", type="string", length=64, nullable=false, options={"fixed" = true})
     *
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(name="nickname", type="string", length=64, nullable=false)
     *
     * @var string
     */
    private $nickname;

    /**
     * @ORM\Column(name="phone", type="string", length=32, nullable=true)
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * Registration constructor.
     */
    public function __construct()
    {
        $this->createdAt =  new DateTime();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Country $country
     *
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     *
     * @return $this
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return $this
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
