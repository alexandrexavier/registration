<?php

namespace AlexandreXavier\Registration\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="registration_country",
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="A_COUNTRY_MUST_BE_UNIQUE", columns={"fk_country"})
 *     }
 * )
 *
 * @package AlexandreXavier\Registration\Entity
 */
class RegistrationCountry
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AlexandreXavier\Registration\Entity\Country")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_country", referencedColumnName="id", nullable=false, unique=true)
     * })
     *
     * @var \AlexandreXavier\Registration\Entity\Country
     */
    private $country;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \AlexandreXavier\Registration\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Country $country
     *
     * @return $this
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
