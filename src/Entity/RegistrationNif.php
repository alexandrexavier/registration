<?php

namespace AlexandreXavier\Registration\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(
 *     name="registration_nif",
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(name="A_REGISTRATION_MUST_BE_UNIQUE", columns={"fk_registration"}),
 *      @ORM\UniqueConstraint(name="A_NIF_MUST_BE_UNIQUE", columns={"nif"}),
 *     }
 * )
 *
 * @package AlexandreXavier\Registration\Entity
 */
class RegistrationNif
{
    /**
     * @ORM\Id
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     *
     * @var integer
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="AlexandreXavier\Registration\Entity\Registration")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_registration", referencedColumnName="id", nullable=false, unique=true)
     * })
     *
     * @var \AlexandreXavier\Registration\Entity\Registration
     */
    private $registration;

    /**
     * @ORM\Column(name="nif", type="string", length=45, nullable=true)
     *
     * @var string
     */
    private $nif;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     *
     * @var \DateTime
     */
    private $createdAt;


    /**
     * Nif constructor
     *
     * @param \AlexandreXavier\Registration\Entity\Registration
     */
    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
        $this->createdAt = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return \AlexandreXavier\Registration\Entity\Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param \AlexandreXavier\Registration\Entity\Registration $registration
     *
     * @return $this
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;

        return $this;
    }

    /**
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * @param string $nif
     *
     * @return $this
     */
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
