# How to Setup

#### 0. Requirements
* PHP >= 5.5.38
* Composer >= 1.4.2
* MySQL >= 5.6.35

#### 1. Install the dependencies

First of all you must install the dependencies by running `composer install` in your project root path

#### 2. Setup the database connection

Copy template file by running

```
cp config/database.yml.dist config/database.yml
```

Put your database info in this file (config/database.yml)

#### 3. Importing database
Import database by running

```
php vendor/bin/doctrine dbal:import app/migration/schema.sql 
```
# Requirements

Candidates must develop in HTML+CSS the lead caption form suggested in the mockup provided.

The form should replicate exactly the one present in the given reference.

Candidate must provide SQL for DB creation, HTML + CSS + JS and PHP.

Specification:

- [x] Form validation and development must be coded in Javascript + PHP

- Upon form fulfilment, a message must be presented stating if it was a success or indication
possible problems.

- A check for email already in use must be implemented without the need to submit the form.
- Please validate fields:
o NIF  Nine numeric digits
o Postal Code  XXXX – XXX
o Phone  If Country is Portugal should only allow PT phone numbers
- Password strength algorithm can be defined by the candidate and explained in code comments.
- Security and code efficiency are important evaluation points.
- The solution presented should be a working solution.
- Colors/Design presented is not mandatory. Candidates may disregard for instance colors of
elements.
- The country list can be filled only with 4 countries as long one of them is Portugal.