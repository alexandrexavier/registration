<?php

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/
require_once __DIR__
    . DIRECTORY_SEPARATOR . '..'
    . DIRECTORY_SEPARATOR . 'vendor'
    . DIRECTORY_SEPARATOR . 'autoload.php';

/*
|--------------------------------------------------------------------------
| Build app to get the Entity Manager
|--------------------------------------------------------------------------
|
| This trait will build app to create the doctrine entity manager
|
*/
use AlexandreXavier\App\Bootstrap;
use Doctrine\ORM\Tools\Console\ConsoleRunner;
use League\Container\Container;

return ConsoleRunner::createHelperSet(
    (new Bootstrap(new Container()))->getEntityManager()
);
