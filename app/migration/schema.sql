CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iso_code` char(2) NOT NULL,
  `en_name` varchar(64) NOT NULL,
  `native_name` varchar(64) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `A_COUNTRY_ISO_CODE_MUST_BE_UNIQUE` (`iso_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO country
(`iso_code`, `en_name`, `native_name`, `created_at`)
VALUES
  ( 'AO', 'Angola', 'Angola', NOW()),
  ('BR', 'Brazil', 'Brasil', NOW()),
  ('CV', 'Cape Verde', 'Cabo Verde', NOW()),
  ('PT', 'Portugal', 'Portugal', NOW())
;

CREATE TABLE `registration_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_country` int(10) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `A_COUNTRY_MUST_BE_UNIQUE` (`fk_country`),
  CONSTRAINT `registration_country_fk_country` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO registration_country
  (fk_country, created_at)
SELECT
  id,
  NOW() AS created_at
FROM
  country
;

CREATE TABLE `registration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_country` int(10) unsigned NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `A_REGISTRATION_EMAIL_MUST_BE_UNIQUE` (`email`),
  KEY `registration_idx` (`fk_country`),
  KEY `email_password` (`email`,`password`) USING BTREE,
  CONSTRAINT `registration_fk_country` FOREIGN KEY (`fk_country`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `registration_nif` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `fk_registration` int(10) unsigned NOT NULL,
  `nif` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `A_REGISTRATION_MUST_BE_UNIQUE` (`fk_registration`),
  UNIQUE KEY `A_NIF_MUST_BE_UNIQUE` (`nif`),
  CONSTRAINT `registration_nif_fk_registration` FOREIGN KEY (`fk_registration`) REFERENCES `registration` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;

CREATE TABLE `address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fk_registration` int(10) unsigned NOT NULL,
  `address_line` varchar(255) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `postcode` varchar(32) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `address_fk_registration_idx` (`fk_registration`),
  CONSTRAINT `address_fk_registration` FOREIGN KEY (`fk_registration`) REFERENCES `registration` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;
