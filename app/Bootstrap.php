<?php

namespace AlexandreXavier\App;

use AlexandreXavier\Registration\Controller\CountryController;
use AlexandreXavier\Registration\Controller\IndexController;
use AlexandreXavier\Registration\Controller\RegistrationController;
use AlexandreXavier\Registration\Entity\Country;
use AlexandreXavier\Registration\Entity\Registration;
use AlexandreXavier\Registration\Service\RegistrationBuilder;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use League\Container\ContainerInterface;
use League\Route\RouteCollection;
use League\Route\Strategy\ApplicationStrategy;
use League\Route\Strategy\JsonStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Yaml\Yaml;
use Zend\Diactoros\Response;
use Zend\Diactoros\Response\SapiEmitter;
use Zend\Diactoros\ServerRequestFactory;

/**
 * Uses Doctrine as ORM and League Route as HTTP Router
 *
 * @package AlexandreXavier\App
 */
class Bootstrap
{
    /**
     * @var \League\Container\ContainerInterface
     */
    private $container;

    /**
     * @var \League\Route\RouteCollection
     */
    private $routeCollection;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @param \League\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->entityManager = $this->buildEntityManager();
        $this->registerServicesIntoContainer();
    }

    /**
     * @return \League\Container\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     *
     * @throws \Symfony\Component\Yaml\Exception\ParseException
     */
    private function buildEntityManager()
    {
        $dbParams = Yaml::parse(
            file_get_contents(
                "{$this->getConfigDirectory()}database.yml"
            )
        );

        $config = Setup::createAnnotationMetadataConfiguration(
            [$this->getEntityPath()],
            true,
            null,
            null,
            false
        );

        return EntityManager::create($dbParams, $config);
    }

    /**
     * Register services into container
     */
    private function registerServicesIntoContainer()
    {
        $this->container->share('entity_manager', $this->entityManager);

        $this->container->share(
            'repo.country',
            $this->entityManager->getRepository(Country::class)
        );

        $this->container->share(
            'repo.registration',
            $this->entityManager->getRepository(Registration::class)
        );

        $this->container->share(
            'registration_builder',
            new RegistrationBuilder($this->container->get('repo.country'))
        );
    }

    /**
     * Setup HTTP routes and register routes
     *
     * @return $this
     */
    public function webSetup()
    {
        $this->httpSetUp();
        $this->routeMapping();

        return $this;
    }

    /**
     * Handle Request them emit response
     */
    public function handleHttpRequest()
    {
        $response = $this->routeCollection->dispatch(
            $this->container->get('request'),
            $this->container->get('response')
        );

        return $this->container->get('emitter')->emit($response);
    }

    /**
     * Setting up the application to handle HTTP messages according to PSR
     */
    private function httpSetUp()
    {
        $this->routeCollection = new RouteCollection($this->container);
        $this->routeCollection->setStrategy(new JsonStrategy());

        $this->container->share('response', Response::class);

        $this->container->share('request', function () {
            return ServerRequestFactory::fromGlobals();
        });

        $this->container->share('emitter', SapiEmitter::class);
    }

    /**
     * The application routes goes here
     */
    private function routeMapping()
    {
        $indexAction = function (ServerRequestInterface $request, ResponseInterface $response) {
            return (new IndexController($this->container, $request, $response, $this->getViewDirectory()))
                ->indexAction();
        };

        $this->routeCollection->get('/', $indexAction)
            ->setStrategy(new ApplicationStrategy());

        $this->routeCollection->get(
            '/api/country/registration',
            function (ServerRequestInterface $request, ResponseInterface $response) {
                return (new CountryController($this->container, $request, $response, $this->getViewDirectory()))
                    ->registrationAction();
            }
        );

        $this->routeCollection->get(
            '/api/registration/email/{email}',
            function (ServerRequestInterface $request, ResponseInterface $response, array $args) {
                $registrationController = new RegistrationController(
                    $this->container,
                    $request,
                    $response,
                    $this->getViewDirectory()
                );

                return $registrationController->checkEmailAction($args['email']);
            }
        );

        $this->routeCollection->post(
            '/api/registration',
            function (ServerRequestInterface $request, ResponseInterface $response) {
                $registrationController = new RegistrationController(
                    $this->container,
                    $request,
                    $response,
                    $this->getViewDirectory()
                );

                return $registrationController->postIndexAction();
            }
        );
    }

    /**
     * @return string
     */
    private function getEntityPath()
    {
        return $this->getRootDirectory() . 'src' . DIRECTORY_SEPARATOR . 'Entity';
    }

    /**
     * @return string
     */
    private function getViewDirectory()
    {
        return $this->getRootDirectory() . 'src'
            . DIRECTORY_SEPARATOR . 'Resources'
            . DIRECTORY_SEPARATOR . 'views'
            . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    private function getConfigDirectory()
    {
        return $this->getRootDirectory() . 'config' . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    private function getRootDirectory()
    {
        return realpath(__DIR__ . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR;
    }
}
